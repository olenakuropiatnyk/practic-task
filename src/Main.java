import java.util.Scanner;

public class Main {



    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("1.Режим визуализаци алгоритма:");
        System.out.println("2.Режим изучения алгоритмической сложности");
        System.out.println("Выберите режим:");
        int choice = in.nextInt();
        switch (choice){
            case 1:{
                robota();
                break;
            }
            case 2:{
                slozhnost();
                break;
            }
            default:
                System.out.println("Incorrect input");
        }

        in.close();
    }
    public static void robota(){

        int count;
        int max;
        int min;

        Scanner in = new Scanner(System.in);

        System.out.println("Введите количество элементов: ");
        count = in.nextInt();
        System.out.println("Введите максимальное значение элементов: ");
        max = in.nextInt();
        System.out.println("Введите минимальное значение элементов: ");
        min = in.nextInt();
        ArrayForSort currentArray = new ArrayForSort(count,max, min);

        in.close();
    }
    public static void slozhnost(){
        int max;
        int maxCount;
        int min;
        int minCount;

        Scanner in = new Scanner(System.in);

        System.out.println("Введите максимальное количество элементов: ");
        maxCount = in.nextInt();
        System.out.println("Введите минимальное количество элементов: ");
        minCount = in.nextInt();
        System.out.println("Введите максимальное значение элементов: ");
        max = in.nextInt();
        System.out.println("Введите минимальное значение элементов: ");
        min = in.nextInt();
        ArrayForSort currentArray = new ArrayForSort(maxCount, max, minCount, min);

        in.close();
    }
}
