import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ArrayForSort {
    private int count = 0;
    private int max = 0;
    private int maxCount = 0;
    private int min = 0;
    private int minCount = 0;
    private int[] currArr;
    boolean show = false;
    int m;
    int choice = 0;
    int counter = 0;
    List<Integer> counterList;

    ArrayForSort(int count, int max, int min){
        this.count = count;
        this.max = max+1;
        this.min = min;
        show = true;
        setArr();
        startSort();
    }
    ArrayForSort(int maxCount, int max, int minCount, int min){
        this.maxCount = maxCount;
        this.max = max;
        this.minCount = minCount;
        this.min = min;
        this.count = minCount;
        counterList= new ArrayList<>();
        show = false;
        Scanner in = new Scanner(System.in);

        System.out.println("Выберите алгоритм сортировки: ");
        System.out.println("1 - быстрая");
        System.out.println("2 - слиянием");
        System.out.println("3 - поразрядная");
        this.choice = in.nextInt();
        System.out.println("Количество повторений: ");
        this.m = in.nextInt();
        for (int i = 0; i< m; i++) startExperiment();
    }

    private void setArr(){
        if(show)System.out.print("Изначальный массив: [ ");
        currArr = new int[count];
        for(int i = 0; i < count; i++){
            currArr[i] = (int)(min+((max-min)*Math.random()));
            if(show){
                if(i+1!=count) {
                    System.out.print(currArr[i] + ", ");
                }
                else System.out.println(currArr[i] + " ]");
            }
        }


    }
    private void startSort(){
        if(choice == 0){Scanner in = new Scanner(System.in);

        System.out.println("Выберите алгоритм сортировки: ");
        System.out.println("1 - быстрая");
        System.out.println("2 - слиянием");
        System.out.println("3 - поразрядная");
        choice = in.nextInt();
        in.close();}
        switch (choice) {
            case 1 -> {
                quickSort(currArr, 0, currArr.length - 1);
                break;
            }
            case 2 -> {
                sliyanieSort(currArr, 0, currArr.length - 1);
                break;
            }
            case 3 -> {
                radixSort(currArr);
                break;
            }
            default -> System.out.println("Incorrect input");
        }
    }
    private void save(){
        try(FileWriter writer = new FileWriter("result.txt", true))
        {
            writer.write(counterList.toString());
            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }
    private void showArray(int[] array){
        System.out.print("[ ");
        for(int element = 0; element < array.length; element++){
            if(element+1!=array.length) {
                System.out.print(array[element] + ", ");
            }
            else System.out.println(array[element] + " ]");
        }
    }
    private void startExperiment(){
        while (count <= maxCount){
            for(int i = 0; i<m; i++) {
                setArr();
                startSort();
            }
            counter /= m;
            counterList.add(counter);
            counter = 0;
            count += 10;
        }

        count = minCount;
        System.out.println(counterList.toString());
        save();
        counterList.clear();
    }

    private void quickSort(int[] array, int low, int high) {
        if (array.length == 0)
            return;

        if (low >= high)
            return;

        int middle = low + (high - low) / 2;
        int opora = array[middle];

        int i = low, j = high;
        while (i <= j) {
            while (array[i] < opora) {
                i++;
            }

            while (array[j] > opora) {
                j--;
            }

            if (i <= j) {
                if(show) System.out.println(array[i] + " >= " + array[j]);
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
            else if(show)System.out.println("pivot in correct place");
            if(show)showArray(array);
            else counter++;
        }

        if (low < j)
            quickSort(array, low, j);

        if (high > i)
            quickSort(array, i, high);
    }
    private void sliyanieSort(int[] array, int low, int high) {

        if (high <= low)
            return;
        int mid = low + (high - low) / 2;
        sliyanieSort(array, low, mid);
        sliyanieSort(array, mid + 1, high);

        int[] buf = Arrays.copyOf(array, array.length);
        for (int k = low; k <= high; k++)
            buf[k] = array[k];

        int i = low, j = mid + 1;
        for (int k = low; k <= high; k++) {

            if (i > mid) {
                array[k] = buf[j];
                j++;
            } else if (j > high) {
                array[k] = buf[i];
                i++;
            } else if (buf[j] < buf[i]) {
                array[k] = buf[j];
                j++;
            } else {
                array[k] = buf[i];
                i++;
            }

        }
        if(show)showArray(array);
        else counter++;
    }
    private void radixSort(int[] array) {

        List<Integer>[] buckets = new ArrayList[10];
        for (int i = 0; i < buckets.length; i++) {
            buckets[i] = new ArrayList<Integer>();
        }

        // sort
        boolean flag = false;
        int tmp = -1, divisor = 1;
        while (!flag) {
            flag = true;
            // split array between lists
            for (Integer i : array) {
                tmp = i / divisor;
                buckets[tmp % 10].add(i);
                if (flag && tmp > 0) {
                    flag = false;
                }
            }
            // empty lists into array
            int a = 0;
            for (int b = 0; b < 10; b++) {
                for (Integer i : buckets[b]) {
                    array[a++] = i;
                }
                buckets[b].clear();
            }
            // move to next digit
            divisor *= 10;
            if(show)showArray(array);
            else counter++;
        }
    }
}
